import { Injectable } from '@angular/core';
import { Ficha } from '../model/ficha';
import { Categoria } from '../model/categoria';
import { CategoriaService } from './categoria.service';
import { PacienteService } from './paciente.service';
import { Paciente } from '../model/paciente';

@Injectable({
  providedIn: 'root'
})
export class FichaService {

  private idFichaActual = 1; //auto increment
  constructor(private pacienteService: PacienteService, private categoriaService: CategoriaService) { }

  fichas: Ficha[] = [];

  agregarFicha(ficha: Ficha) {
    const id = sessionStorage.getItem('id_ficha');
    if(id != null){
      ficha.idFicha = JSON.parse(id);
    }else{
      ficha.idFicha = this.idFichaActual;
    }
    
    this.fichas.push(ficha);
    this.idFichaActual++;

    sessionStorage.setItem('fichas', JSON.stringify(this.fichas));
    sessionStorage.setItem('id_ficha', JSON.stringify(this.idFichaActual));
  }

  borrarFicha(idFicha: number) {
    const indice = this.fichas.findIndex(ficha => ficha.idFicha === idFicha);

    if (indice !== -1) {
      this.fichas.splice(indice, 1);
      sessionStorage.removeItem('fichas');
      sessionStorage.setItem('fichas', JSON.stringify(this.fichas));
    }
  }

  obtenerFichas() {
    const fichas_json_str = sessionStorage.getItem('fichas');
    if(fichas_json_str != null){
      this.fichas = JSON.parse(fichas_json_str);
    }
    return this.fichas;
  }

  obtenerFichaPorId(idFicha: number): Ficha | undefined{
    const fichas_json_str = sessionStorage.getItem('fichas');
    if(fichas_json_str != null){
      this.fichas = JSON.parse(fichas_json_str);
    }
    return this.fichas.find(ficha => ficha.idFicha === idFicha);
  }

  modificarFicha(ficha: Ficha): void {
    const indice = this.fichas.findIndex(f => f.idFicha === ficha.idFicha);

    if (indice !== -1) {
      this.fichas[indice] = ficha;
      sessionStorage.removeItem('fichas');
      sessionStorage.setItem('fichas', JSON.stringify(this.fichas));
    }
  }


  obtenerCategoriasDisponibles(): Categoria[] {
    return this.categoriaService.obtenerCategorias();
  }

  obtenerDoctoresDisponibles(): Paciente[] {
    return this.pacienteService.obtenerPacientes().filter(doctor => doctor.flag_es_doctor);
  }

  obtenerPacientesDisponibles(): Paciente[] {
    return this.pacienteService.obtenerPacientes().filter(doctor => !doctor.flag_es_doctor);
  }
}
