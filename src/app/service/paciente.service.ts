import { Injectable } from '@angular/core';
import { Paciente } from '../model/paciente';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  private idPacienteActual = 1; //auto increment
  constructor() { }

  pacientes: Paciente[] = [];

  obtenerPacientes() {
    const pacientes_json_str = sessionStorage.getItem('pacientes');
    if(pacientes_json_str != null){
      this.pacientes = JSON.parse(pacientes_json_str);
    }
    return this.pacientes;
  }

  agregarPaciente(paciente: Paciente) {

    const id = sessionStorage.getItem('id_persona');
    if(id != null){
      paciente.idPersona = JSON.parse(id);
    }else{
      paciente.idPersona = this.idPacienteActual;
    }

    this.pacientes.push(paciente);
    this.idPacienteActual++;
    sessionStorage.setItem('pacientes', JSON.stringify(this.pacientes));
    sessionStorage.setItem('id_persona', JSON.stringify(this.idPacienteActual));
  }

  borrarPaciente(idPersona: number) {
    const indice = this.pacientes.findIndex(paciente => paciente.idPersona === idPersona);

    if (indice !== -1) {
      this.pacientes.splice(indice, 1);
      sessionStorage.removeItem('pacientes');
      sessionStorage.setItem('pacientes', JSON.stringify(this.pacientes));
    }
  }

  obtenerPacientePorId(idPersona: number): Paciente | undefined{
    const pacientes_json_str = sessionStorage.getItem('pacientes');
    if(pacientes_json_str != null){
      this.pacientes = JSON.parse(pacientes_json_str);
    }
    return this.pacientes.find(paciente => paciente.idPersona === idPersona);
  }

  modificarPaciente(paciente: Paciente): void {
    const indice = this.pacientes.findIndex(p => p.idPersona === paciente.idPersona);

    if (indice !== -1) {
      this.pacientes[indice] = paciente;
      sessionStorage.removeItem('pacientes');
      sessionStorage.setItem('pacientes', JSON.stringify(this.pacientes));
    }
  }
}
