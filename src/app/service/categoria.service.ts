import { Injectable, OnInit } from '@angular/core';
import { Categoria } from '../model/categoria';


@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private idCategoriaActual = 1; //auto increment
  constructor() { }

  categorias: Categoria[] = [];

  agregarCategoria(categoria: Categoria) {
    const id = sessionStorage.getItem('id_categoria');
    if(id != null){
      categoria.idCategoria = JSON.parse(id);
    }else{
      categoria.idCategoria = this.idCategoriaActual;
    }
    this.categorias.push(categoria);
    this.idCategoriaActual++;
    sessionStorage.setItem('categorias', JSON.stringify(this.categorias));
    sessionStorage.setItem('id_categoria', JSON.stringify(this.idCategoriaActual));
  }

  borrarCategoria(idCategoria: number) {
    const indice = this.categorias.findIndex(categoria => categoria.idCategoria === idCategoria);

    if (indice !== -1) {
      this.categorias.splice(indice, 1);
      sessionStorage.removeItem('categorias');
      sessionStorage.setItem('categorias', JSON.stringify(this.categorias));
    }
  }

  obtenerCategorias() {
    const categorias_json_str = sessionStorage.getItem('categorias');
    if(categorias_json_str != null){
      this.categorias = JSON.parse(categorias_json_str);
    }
    return this.categorias;
  }

  obtenerCategoriaPorId(idCategoria: number): Categoria | undefined{
    const categorias_json_str = sessionStorage.getItem('categorias');
    if(categorias_json_str != null){
      this.categorias = JSON.parse(categorias_json_str);
    }
    return this.categorias.find(categoria => categoria.idCategoria === idCategoria);
  }

  modificarCategoria(categoria: Categoria): void {
    const indice = this.categorias.findIndex(c => c.idCategoria === categoria.idCategoria);

    if (indice !== -1) {
      this.categorias[indice] = categoria;
      sessionStorage.removeItem('categorias');
      sessionStorage.setItem('categorias', JSON.stringify(this.categorias));
    }
  }

}
