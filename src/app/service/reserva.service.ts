import { Injectable } from '@angular/core';
import { Reserva } from '../model/reserva';
import { Paciente } from '../model/paciente';
import { PacienteService } from '../service/paciente.service';

@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  private idReservaActual = 1; //auto increment
  constructor(private pacienteService: PacienteService) { }

  reservas: Reserva[] = [];

  agregarReserva(reserva: Reserva) {
    const id = sessionStorage.getItem('id_reserva');
    if(id != null){
      reserva.idReserva = JSON.parse(id);
    }else{
      reserva.idReserva = this.idReservaActual;
    }

    this.reservas.push(reserva);
    this.idReservaActual++;

    sessionStorage.setItem('reservas', JSON.stringify(this.reservas));
    sessionStorage.setItem('id_reserva', JSON.stringify(this.idReservaActual));
  }

  borrarReserva(idReserva: number) {
    const indice = this.reservas.findIndex(reserva => reserva.idReserva === idReserva);

    if (indice !== -1) {
      this.reservas.splice(indice, 1);
      sessionStorage.removeItem('reservas');
      sessionStorage.setItem('reservas', JSON.stringify(this.reservas));
    }
  }

  obtenerReservas() {
    const reservas_json_str = sessionStorage.getItem('reservas');
    if(reservas_json_str != null){
      this.reservas = JSON.parse(reservas_json_str);
    }
    return this.reservas;
  }

  obtenerReservaPorId(idReserva: number): Reserva | undefined{
    const reservas_json_str = sessionStorage.getItem('reservas');
    if(reservas_json_str != null){
      this.reservas = JSON.parse(reservas_json_str);
    }
    return this.reservas.find(reserva => reserva.idReserva === idReserva);
  }

  modificarReserva(reserva: Reserva): void {
    const indice = this.reservas.findIndex(c => c.idReserva === reserva.idReserva);

    if (indice !== -1) {
      this.reservas[indice] = reserva;
      sessionStorage.removeItem('reservas');
      sessionStorage.setItem('reservas', JSON.stringify(this.reservas));
    }
  }

  private horasDisponibles: string[] = [
    '09:00 a 10:00',
    '10:00 a 11:00',
    '11:00 a 12:00',
    '12:00 a 13:00',
    '13:00 a 14:00',
    '14:00 a 15:00',
    '15:00 a 16:00',
    '16:00 a 17:00',
    '17:00 a 18:00',
    '18:00 a 19:00',
    '19:00 a 20:00',
    '20:00 a 21:00'
  ];

  obtenerHorasDisponibles(): string[] {
    return this.horasDisponibles;
  }

  obtenerDoctoresDisponibles(): Paciente[] {
    return this.pacienteService.obtenerPacientes().filter(doctor => doctor.flag_es_doctor);
  }

  obtenerPacientesDisponibles(): Paciente[] {
    return this.pacienteService.obtenerPacientes().filter(doctor => !doctor.flag_es_doctor);
  }
}
