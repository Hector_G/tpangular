import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaModificarComponent } from './ficha-modificar.component';

describe('FichaModificarComponent', () => {
  let component: FichaModificarComponent;
  let fixture: ComponentFixture<FichaModificarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FichaModificarComponent]
    });
    fixture = TestBed.createComponent(FichaModificarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
