import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Categoria } from 'src/app/model/categoria';
import { Ficha } from 'src/app/model/ficha';
import { Paciente } from 'src/app/model/paciente';
import { FichaService } from 'src/app/service/ficha.service';

@Component({
  selector: 'app-ficha-modificar',
  templateUrl: './ficha-modificar.component.html',
  styleUrls: ['./ficha-modificar.component.css']
})
export class FichaModificarComponent implements OnInit {
  ficha: Ficha = new Ficha();

  constructor(
    private fichaService: FichaService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  doctoresDisponibles: Paciente[] = [];
  pacientesDisponibles: Paciente[] = [];
  categoriasDisponibles: Categoria[] = [];

  guardarModificacion(): void {
    this.fichaService.modificarFicha(this.ficha);
    this.router.navigate(['/fichaclinica']);
  }

  ngOnInit(): void {

    this.ficha._doctor = new Paciente();
    this.ficha._paciente = new Paciente();
    this.ficha.categoria = new Categoria();
    this.doctoresDisponibles = this.fichaService.obtenerDoctoresDisponibles();
    this.pacientesDisponibles = this.fichaService.obtenerPacientesDisponibles();
    this.categoriasDisponibles = this.fichaService.obtenerCategoriasDisponibles();

    const idFichaParam = this.route.snapshot.paramMap.get('id');
    const idFicha = idFichaParam ? +idFichaParam : 0;
  
    if (idFicha) {
      const fichaEncontrada = this.fichaService.obtenerFichaPorId(idFicha);
  
      if (fichaEncontrada) {
        this.ficha = fichaEncontrada;
      } else {
        console.error('Ficha no encontrada');
      }
    } else {
      console.error('ID de ficha no válido');
    }
  }
}
