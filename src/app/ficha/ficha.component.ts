import { Component, ElementRef, ViewChild } from '@angular/core';
import { Ficha } from '../model/ficha';
import { FichaService } from '../service/ficha.service';
import { jsPDF } from "jspdf";
import autoTable from 'jspdf-autotable';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.css']
})
export class FichaComponent {
  @ViewChild('tabla_ficha') tabla_ficha: ElementRef;

  fichas: Ficha[] = [];
  filtroDoctor: string = '';
  filtroPaciente: string = '';
  filtroCategoria: string = '';
  fechaDesde: string = this.getCurrentDate();
  fechaHasta: string = this.getCurrentDate();

  generateExcel(): void {
    const table = this.tabla_ficha.nativeElement;
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(table);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

    XLSX.writeFile(wb, 'tabla.xlsx');
  }

  generatePDF() {
    const doc = new jsPDF();
    autoTable(doc,{html: '#tabla_ficha',})

    doc.save('reporte_ficha.pdf');
  }

  getCurrentDate(): string {
    const currentDate = new Date();
    const day = currentDate.getDate().toString().padStart(2, '0');
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
    const year = currentDate.getFullYear();
    return `${year}-${month}-${day}`;
  }

  parseDateString(dateString: string): Date {
    const parts = dateString.split('-');
    const year = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10)-1;
    const day = parseInt(parts[2], 10);
    return new Date(year, month, day);
  }

  constructor(private fichaService: FichaService) {
    this.tabla_ficha = new ElementRef(null);
    this.fichas = this.fichaService.obtenerFichas();
  }

  borrarFicha(idFicha: number) {
    this.fichaService.borrarFicha(idFicha);
  }
}
