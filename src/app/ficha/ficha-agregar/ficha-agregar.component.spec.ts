import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaAgregarComponent } from './ficha-agregar.component';

describe('FichaAgregarComponent', () => {
  let component: FichaAgregarComponent;
  let fixture: ComponentFixture<FichaAgregarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FichaAgregarComponent]
    });
    fixture = TestBed.createComponent(FichaAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
