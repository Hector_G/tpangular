import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Categoria } from 'src/app/model/categoria';
import { Ficha } from 'src/app/model/ficha';
import { Paciente } from 'src/app/model/paciente';
import { FichaService } from 'src/app/service/ficha.service';

@Component({
  selector: 'app-ficha-agregar',
  templateUrl: './ficha-agregar.component.html',
  styleUrls: ['./ficha-agregar.component.css']
})
export class FichaAgregarComponent implements OnInit{
  nuevaFicha: Ficha = new Ficha();

  constructor(private fichaService: FichaService, private router: Router) { }

  doctoresDisponibles: Paciente[] = [];
  pacientesDisponibles: Paciente[] = [];
  categoriasDisponibles: Categoria[] = [];

  ngOnInit(): void {
    this.nuevaFicha._doctor = new Paciente();
    this.nuevaFicha._paciente = new Paciente();
    this.nuevaFicha.categoria = new Categoria();
    this.doctoresDisponibles = this.fichaService.obtenerDoctoresDisponibles();
    this.pacientesDisponibles = this.fichaService.obtenerPacientesDisponibles();
    this.categoriasDisponibles = this.fichaService.obtenerCategoriasDisponibles();
  }

  guardarFicha() {
    this.fichaService.agregarFicha(this.nuevaFicha);
    this.router.navigate(['/fichaclinica']);
  }
}
