import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaAgregarReservaComponent } from './ficha-agregar-reserva.component';

describe('FichaAgregarReservaComponent', () => {
  let component: FichaAgregarReservaComponent;
  let fixture: ComponentFixture<FichaAgregarReservaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FichaAgregarReservaComponent]
    });
    fixture = TestBed.createComponent(FichaAgregarReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
