
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Categoria } from 'src/app/model/categoria';
import { Ficha } from 'src/app/model/ficha';
import { Paciente } from 'src/app/model/paciente';
import { Reserva } from 'src/app/model/reserva';
import { FichaService } from 'src/app/service/ficha.service';
import { ReservaService } from 'src/app/service/reserva.service';

@Component({
  selector: 'app-ficha-agregar-reserva',
  templateUrl: './ficha-agregar-reserva.component.html',
  styleUrls: ['./ficha-agregar-reserva.component.css']
})
export class FichaAgregarReservaComponent implements OnInit {
  nuevaFicha: Ficha = new Ficha();

  constructor(private fichaService: FichaService, private reservaService:ReservaService,
    private router: Router) { }

  reservaElegida: Reserva = new Reserva();
  reservasDisponibles: Reserva[] = [];
  categoriasDisponibles: Categoria[] = [];

  ngOnInit(): void {
    this.nuevaFicha._doctor = new Paciente();
    this.nuevaFicha._paciente = new Paciente();
    this.nuevaFicha.categoria = new Categoria();
    this.reservasDisponibles = this.reservaService.obtenerReservas();
    this.categoriasDisponibles = this.fichaService.obtenerCategoriasDisponibles();
  }

  guardarFicha() {
    this.nuevaFicha._doctor = this.reservaElegida._doctor;
    this.nuevaFicha._paciente = this.reservaElegida._paciente;
    this.fichaService.agregarFicha(this.nuevaFicha);
    this.router.navigate(['/fichaclinica']);
  }
}
