import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaModificarComponent } from './categoria-modificar.component';

describe('CategoriaModificarComponent', () => {
  let component: CategoriaModificarComponent;
  let fixture: ComponentFixture<CategoriaModificarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CategoriaModificarComponent]
    });
    fixture = TestBed.createComponent(CategoriaModificarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
