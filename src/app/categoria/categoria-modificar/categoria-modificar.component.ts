import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Categoria } from 'src/app/model/categoria';
import { CategoriaService } from 'src/app/service/categoria.service';

@Component({
  selector: 'app-categoria-modificar',
  templateUrl: './categoria-modificar.component.html',
  styleUrls: ['./categoria-modificar.component.css']
})
export class CategoriaModificarComponent implements OnInit {
  categoria: Categoria = new Categoria();

  constructor(
    private categoriaService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  guardarModificacion(): void {
    this.categoriaService.modificarCategoria(this.categoria);
    this.router.navigate(['/categoria']);
  }

  ngOnInit(): void {
    const idCategoriaParam = this.route.snapshot.paramMap.get('id');
    const idCategoria = idCategoriaParam ? +idCategoriaParam : 0;
  
    if (idCategoria) {
      const categoriaEncontrada = this.categoriaService.obtenerCategoriaPorId(idCategoria);
  
      if (categoriaEncontrada) {
        this.categoria = categoriaEncontrada;
      } else {
        console.error('Categoría no encontrada');
      }
    } else {
      console.error('ID de categoría no válido');
    }
  }
}
