  import { Component } from '@angular/core';
  import { Categoria } from '../model/categoria';
  import { CategoriaService } from '../service/categoria.service';

  @Component({
    selector: 'app-categoria',
    templateUrl: './categoria.component.html',
    styleUrls: ['./categoria.component.css']
  })
  export class CategoriaComponent {
    categorias: Categoria[] = [];

    constructor(private categoriaService: CategoriaService) {
      this.categorias = this.categoriaService.obtenerCategorias();
    }

    borrarCategoria(idCategoria: number) {
      this.categoriaService.borrarCategoria(idCategoria);
    }
  }
