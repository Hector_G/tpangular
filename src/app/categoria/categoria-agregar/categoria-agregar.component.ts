import { Component } from '@angular/core';
import { Categoria } from 'src/app/model/categoria';
import { CategoriaService } from 'src/app/service/categoria.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria-agregar',
  templateUrl: './categoria-agregar.component.html',
  styleUrls: ['./categoria-agregar.component.css']
})
export class CategoriaAgregarComponent {
  nuevaCategoria: Categoria = new Categoria();

  constructor(private categoriaService: CategoriaService, private router: Router) { }

  guardarCategoria() {
    this.categoriaService.agregarCategoria(this.nuevaCategoria);
    this.router.navigate(['/categoria']);
  }
}
