import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente';
import { Reserva } from 'src/app/model/reserva';
import { ReservaService } from 'src/app/service/reserva.service';

@Component({
  selector: 'app-reserva-agregar',
  templateUrl: './reserva-agregar.component.html',
  styleUrls: ['./reserva-agregar.component.css']
})
export class ReservaAgregarComponent implements OnInit{
  nuevaReserva: Reserva = new Reserva();

  doctoresDisponibles: Paciente[] = [];
  pacientesDisponibles: Paciente[] = [];
  horasDisponibles: string[] = [];

  constructor(private reservaService: ReservaService, private router: Router) { }

  ngOnInit(): void {
    this.nuevaReserva._doctor = new Paciente();
    this.nuevaReserva._paciente = new Paciente();
    this.doctoresDisponibles = this.reservaService.obtenerDoctoresDisponibles();
    this.pacientesDisponibles = this.reservaService.obtenerPacientesDisponibles();
    this.horasDisponibles = this.reservaService.obtenerHorasDisponibles();
  }

  guardarReserva() {
    this.reservaService.agregarReserva(this.nuevaReserva);
    this.router.navigate(['/reserva']);
  }
}
