import { Component } from '@angular/core';
import { Reserva } from '../model/reserva';
import { ReservaService } from '../service/reserva.service';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent {
  reservas: Reserva[] = [];
  filtroDoctor: string = '';
  filtroPaciente: string = '';
  fechaDesde: string = this.getCurrentDate();
  fechaHasta: string = this.getCurrentDate();

  getCurrentDate(): string {
    const currentDate = new Date();
    const day = currentDate.getDate().toString().padStart(2, '0');
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
    const year = currentDate.getFullYear();
    return `${year}-${month}-${day}`;
  }

  parseDateString(dateString: string): Date {
    const parts = dateString.split('-');
    const year = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10)-1;
    const day = parseInt(parts[2], 10);
    return new Date(year, month, day);
  }

  constructor(private reservaService: ReservaService) {
    this.reservas = this.reservaService.obtenerReservas();
  }

  borrarReserva(idReserva: number) {
    this.reservaService.borrarReserva(idReserva);
  }
}
