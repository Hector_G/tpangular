import { Categoria } from "./categoria";
import { Paciente } from "./paciente";

export class Ficha {
    idFicha!: number;
    _paciente!: Paciente;
    _doctor!: Paciente;
    fecha!: string;
    categoria!: Categoria;
    motivo_consulta!: string;
    diagnostico!: string;
}
