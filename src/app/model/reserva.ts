import { Paciente } from "./paciente";

export class Reserva {
    idReserva!: number;
    _paciente!: Paciente;
    _doctor!: Paciente;
    fecha!: string;
    hora!: string;
}
