import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriaComponent } from './categoria/categoria.component';
import { CategoriaAgregarComponent } from './categoria/categoria-agregar/categoria-agregar.component';
import { CategoriaModificarComponent } from './categoria/categoria-modificar/categoria-modificar.component';
import { PacienteComponent } from './paciente/paciente.component';
import { PacienteAgregarComponent } from './paciente/paciente-agregar/paciente-agregar.component';
import { PacienteModificarComponent } from './paciente/paciente-modificar/paciente-modificar.component';
import { ReservaComponent } from './reserva/reserva.component';
import { ReservaAgregarComponent } from './reserva/reserva-agregar/reserva-agregar.component';
import { FichaComponent } from './ficha/ficha.component';
import { FichaAgregarComponent } from './ficha/ficha-agregar/ficha-agregar.component';
import { FichaModificarComponent } from './ficha/ficha-modificar/ficha-modificar.component';
import { FichaAgregarReservaComponent } from './ficha/ficha-agregar-reserva/ficha-agregar-reserva.component';

const routes: Routes = [
  {
    path:'categoria',
    component: CategoriaComponent
  },
  {
    path:'nuevacategoria',
    component: CategoriaAgregarComponent
  },
  {
    path: 'modificarcategoria/:id',
    component: CategoriaModificarComponent
  },
  {
    path:'paciente',
    component: PacienteComponent
  },
  {
    path:'nuevopaciente',
    component: PacienteAgregarComponent
  },
  {
    path:'modificarpaciente/:id',
    component: PacienteModificarComponent
  },
  {
    path:'reserva',
    component: ReservaComponent
  },
  {
    path:'nuevareserva',
    component: ReservaAgregarComponent
  },
  {
    path:'fichaclinica',
    component: FichaComponent
  },
  {
    path:'nuevafichaclinica',
    component: FichaAgregarComponent
  },
  {
    path:'nuevafichaclinica_reserva',
    component: FichaAgregarReservaComponent
  },
  {
    path:'modificarfichaclinica/:id',
    component: FichaModificarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
