import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { CategoriaAgregarComponent } from './categoria/categoria-agregar/categoria-agregar.component';
import { FormsModule } from '@angular/forms';
import { CategoriaModificarComponent } from './categoria/categoria-modificar/categoria-modificar.component';
import { PacienteComponent } from './paciente/paciente.component';
import { PacienteAgregarComponent } from './paciente/paciente-agregar/paciente-agregar.component';
import { PacienteModificarComponent } from './paciente/paciente-modificar/paciente-modificar.component';
import { ReservaComponent } from './reserva/reserva.component';
import { ReservaAgregarComponent } from './reserva/reserva-agregar/reserva-agregar.component';
import { FichaComponent } from './ficha/ficha.component';
import { FichaAgregarComponent } from './ficha/ficha-agregar/ficha-agregar.component';
import { FichaModificarComponent } from './ficha/ficha-modificar/ficha-modificar.component';
import { FichaAgregarReservaComponent } from './ficha/ficha-agregar-reserva/ficha-agregar-reserva.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriaComponent,
    CategoriaAgregarComponent,
    CategoriaModificarComponent,
    PacienteComponent,
    PacienteAgregarComponent,
    PacienteModificarComponent,
    ReservaComponent,
    ReservaAgregarComponent,
    FichaComponent,
    FichaAgregarComponent,
    FichaModificarComponent,
    FichaAgregarReservaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
