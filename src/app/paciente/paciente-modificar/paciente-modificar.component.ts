import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente';
import { PacienteService } from 'src/app/service/paciente.service';

@Component({
  selector: 'app-paciente-modificar',
  templateUrl: './paciente-modificar.component.html',
  styleUrls: ['./paciente-modificar.component.css']
})
export class PacienteModificarComponent {
  paciente: Paciente = new Paciente();

  constructor(
    private pacienteService: PacienteService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  guardarModificacion(): void {
    this.pacienteService.modificarPaciente(this.paciente);
    this.router.navigate(['/paciente']);
  }

  ngOnInit(): void {
    const idPersonaParam = this.route.snapshot.paramMap.get('id');
    const idPersona = idPersonaParam ? +idPersonaParam : 0;
  
    if (idPersona) {
      const pacienteEncontrado = this.pacienteService.obtenerPacientePorId(idPersona);
  
      if (pacienteEncontrado) {
        this.paciente = pacienteEncontrado;
      } else {
        console.error('Paciente/Doctor no encontrado');
      }
    } else {
      console.error('ID de Paciente/Doctor no válido');
    }
  }
}
