import { Component } from '@angular/core';
import { Paciente } from '../model/paciente';
import { PacienteService } from '../service/paciente.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent {
  pacientes: Paciente[] = [];
  filtroTipo: string = 'Todos';
  filtroNombre: string = '';
  filtroApellido: string = '';

  constructor(private pacienteService: PacienteService) {
    this.pacientes = this.pacienteService.obtenerPacientes();
  }

  borrarPaciente(idPersona: number) {
    this.pacienteService.borrarPaciente(idPersona);
  }


}
