import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente';
import { PacienteService } from 'src/app/service/paciente.service';

@Component({
  selector: 'app-paciente-agregar',
  templateUrl: './paciente-agregar.component.html',
  styleUrls: ['./paciente-agregar.component.css']
})
export class PacienteAgregarComponent {
  nuevoPaciente: Paciente = new Paciente();

  constructor(private pacienteService: PacienteService, private router: Router) { }

  guardarPaciente() {
    this.pacienteService.agregarPaciente(this.nuevoPaciente);
    this.router.navigate(['/paciente']);
  }
}
